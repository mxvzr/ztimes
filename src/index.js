var assert = require('assert'),
	lib = require('./lib'),
	Interval = require('@lnmx/iso8601').Interval;

/**
 * @param {Object} client	Redis client
 * @param {String} scheme	util.format's format string
 * @param {String|Interval} interval
 * @param {Date} epoch
 * @param {Number|String|Interval} TTL
 */
function ZTimes(client, scheme, interval, epoch, TTL) {
	if (!(this instanceof ZTimes))
		return new ZTimes(client, scheme, interval, epoch, TTL);

	assert(client && client.multi && typeof client.multi === 'function', 'Invalid redis client');
	assert(scheme && typeof scheme === 'string' && scheme.indexOf('%s') > -1, 'Invalid naming scheme');
	
	interval = interval instanceof Interval ? interval : new Interval(interval);
	assert(!isNaN(+interval), 'Invalid interval');

	epoch = epoch instanceof Date ? epoch : new Date(epoch);
	assert(!isNaN(epoch.getTime()), 'Invalid epoch');

	TTL = TTL || 0;
	if (typeof TTL === 'string')
		TTL = new Interval(TTL);
	if (TTL instanceof Interval)
		TTL = +TTL;
	assert(!isNaN(TTL), 'Invalid TTL');
	
	var periodSharder = lib.intervalSinceEpoch(+interval, epoch),
		keyMaker = lib.getKey(scheme);

	Object.defineProperty(this, 'incr', {
		value: lib.increment(client, periodSharder, keyMaker, TTL), // identity [, amount [, time ] ]
	});

	Object.defineProperty(this, 'get', {
		value: lib.get(client, periodSharder, keyMaker, interval) // from, to [, identity]
	});
};

module.exports = ZTimes;