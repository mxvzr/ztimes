var assert = require('assert'),
	util = require('util'),
	R = require('ramda'),
	Bluebird = require('bluebird'),
	uuidgen = require('node-uuid'),
	Period = require('@lnmx/iso8601').Period,
	Duration = require('@lnmx/iso8601').Duration;

var intervalSinceEpoch = R.curry(function(interval, epoch, ts) {
	if (ts instanceof Date)
		ts = ts.getTime();
	if (epoch instanceof Date)
		epoch = epoch.getTime();
	assert(typeof ts === 'number' && !isNaN(ts), 'Invalid timestamp');
	assert(typeof epoch === 'number' && !isNaN(epoch), 'Invalid epoch');
	assert(typeof interval === 'number' && interval > 0, 'Invalid interval');
	return Math.floor(((ts-epoch)/interval));
});

var getKey = R.curry(function(scheme, period) {
	return util.format(scheme, period);
});

var execMulti = R.curry(function(client, cmds) {
	return new Bluebird(function(resolve, reject) {
		client.multi(cmds).exec(function(err, replies) {
			if (err) {
				reject(err);
			} else {
				resolve(replies);
			}
		});
	});
});

var getRange = function(periodSharder, keyMaker, interval, from, to) {
	var range = new Period(new Duration(from, to), interval);
	return R.map(R.compose(keyMaker, periodSharder), range);
};

var zipValueScores = function(result) {
	var retVal = {};
	for (var i = 0; i < result.length; i = i + 2)
		retVal[result[i]] = result[i + 1];
	return retVal;
};

function get(client, periodSharder, keyMaker, interval) {
	return function(from, to, identity) {
		var keys = getRange(periodSharder, keyMaker, interval, from, to),
			cmds;

		if (identity) {
			cmds = keys.map(function(k) {
				return ['ZSCORE', k, identity];
			});
		} else {
			var tmp = 'zt:' + uuidgen.v4();
			cmds =  [
				['ZUNIONSTORE', tmp, keys.length].concat(keys).concat(['AGGREGATE', 'SUM']),
				['ZRANGE', tmp, 0, -1, 'WITHSCORES'],
				['DEL', tmp]
			];
		}

		return execMulti(client, cmds).then(function(results) {
			if (identity) {
				return results.reduce(function(acc, result) {
					return acc += Number(result) || 0;
				}, 0);
			} else {
				return zipValueScores(results[1]);
			}
		});
	};
}

function increment(client, periodSharder, keyMaker, TTL) {
	return function(identity, incrValue, time) {
		var key = keyMaker(periodSharder(time || new Date())),
			cmds = [
				['ZINCRBY', key, incrValue || 1, identity]
			];

		if (TTL)
			cmds.push(['PEXPIRE', key, TTL]);

		return execMulti(client, cmds).then(function(results) {
			return results[0];
		});
	};
}

module.exports = {
	intervalSinceEpoch: intervalSinceEpoch,
	getKey: getKey,
	getRange: getRange,
	execMulti: execMulti,
	zipValueScores: zipValueScores,
	get: get,
	increment: increment
};