all: test coverage

test:
	NODE_ENV=test BLUEBIRD_DEBUG=1 ./node_modules/.bin/mocha -R spec --recursive tests/

coverage:
	NODE_ENV=test BLUEBIRD_DEBUG=1 ./node_modules/.bin/istanbul cover --print detail ./node_modules/.bin/_mocha -- -R dot --recursive tests/

.PHONY: all test coverage
