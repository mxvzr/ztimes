## ztimes - redis stored time series

# incr(id [, value [, datetime ] ])
	- uses ZINCRYBY [& PEXIRE]
# get(from, to [, id ])
	- N = number of slices between from & to
	- uses 1 ZUNIONSTORE & 1 ZRANGE when id is null; O(beaucoup(N))
	- uses N ZSCORE otherwise; O(N)

# example
```
#!javascript
var P = require('bluebird'),
	ZT = require('./src');

const ONE_HOUR = 1000 * 60 * 60;
const SIX_HOURS_FROM_NOW = new Date(Date.now() + (6 * ONE_HOUR));
const ONE_DAY_FROM_NOW = new Date(Date.now() + (24 * ONE_HOUR));

var client = require('redis').createClient(), // redis client
	scheme = 'example:count:%s', // key naming format
	interval = 'PT1H', // duration of each slice
	epoch = new Date('2014-01-01'), // epoch used when counting slices
	TTL = 'PT5S'; // max lifetime of each slice in redis

var zcount = new ZT(client, scheme, interval, epoch, TTL);

P.cast().then(function() {
	return zcount.incr('uuid'); // +1 to 'p:caps:0'
}).then(function(result) {
	console.log(result, result == 1);
	return zcount.incr('uuid', 20); // + 20 to 'p:caps:0' 
}).then(function(result) {
	console.log(result, result == 21);
	return zcount.incr('uuid', 5, SIX_HOURS_FROM_NOW); // + 5 in 'p:caps:5'
}).then(function(result) {
	console.log(result, result == 5);
	return zcount.get(new Date(), ONE_DAY_FROM_NOW, 'uuid'); // === 26 (1 + 20 + 5);
}).then(function(result) {
	console.log(result, result == 26);
	return P.join(
		zcount.incr('foo'),
		zcount.incr('bar'),
		zcount.incr('tek')
	).then(function() {
		return zcount.get(new Date(), ONE_DAY_FROM_NOW);
	});
}).then(function(result) {
	console.log(result);
	process.exit();
});
```
