var assert = require('assert'),
	redis = require('redis').createClient(),
	lib = require('../src/lib'),
	Interval = require('@lnmx/iso8601').Interval;

describe('/src/lib', function() {
	describe('#zipValueScores', function() {
		it('transforms Z command results w/ scores to objects', function() {
			var zResult = [
				'one', 1,
				'two', 2,
				'three', 3
			];
			var result = lib.zipValueScores(zResult);
			assert(result.one && result.one === 1);
			assert(result.two && result.two === 2);
			assert(result.three && result.three === 3);
		});
	});

	describe('#execMulti', function() {
		it('is properly defined', function() {
			assert.equal(typeof lib.execMulti, 'function');
			assert.equal(lib.execMulti.length, 2);
		});
		it('runs redis commands passed in an array', function(done) {
			lib.execMulti(redis, [
				['SET', 'execMulti:test', 1],
				['INCRBY', 'execMulti:test', 2],
				['GET', 'execMulti:test'],
				['DEL', 'execMulti:test']
			]).then(function(replies) {
				assert(Array.isArray(replies));
				assert.equal(replies.length, 4);
				assert.equal(replies[0], 'OK');
				assert.equal(replies[1], 3);
				assert.equal(replies[2], 3);
				assert.equal(replies[3], 1);
				done();
			}).catch(done);
		});
	});

	describe('#intervalSinceEpoch', function() {
		it('is properly defined', function(){
			assert.equal(typeof lib.intervalSinceEpoch, 'function');
			assert.equal(lib.intervalSinceEpoch.length, 3);
		});

		it('is curried', function() {
			assert.equal(lib.intervalSinceEpoch(1).length, 2);
			assert.equal(lib.intervalSinceEpoch(1, new Date()).length, 1);
		});

		it('returns the difference in "interval" between "epoch" and "ts"', function() {
			var oneDayInterval = 1000 * 60 * 60 * 24,
				epoch = new Date(),
				daysSinceToday = lib.intervalSinceEpoch(oneDayInterval, epoch);

			assert.equal(daysSinceToday(new Date(+Date.now() + (2 * oneDayInterval))), 2); 
			assert.equal(daysSinceToday(new Date(+Date.now() + (4 * oneDayInterval))), 4);
		});
	});

	describe('#getKey', function() {
		it('is properly defined', function() {
			assert.equal(typeof lib.getKey, 'function');
			assert.equal(lib.getKey.length, 2);
		});

		it('is curried', function() {
			assert.equal(lib.getKey('foo').length, 1);
		});

		it('returns a formatted string', function() {
			assert.equal(lib.getKey('foo:%s', 1), 'foo:1');
		});
	});

	describe('#increment', function() {
		before(function(done) {
			redis.keys('ztime:incr_test:*', function(err, keys) {
				if (err) {
					done(err);
				} else if (keys && keys.length) {
					redis.del(keys, done);
				} else {
					done();
				}
			});
		});

		const oneDayInterval = 1000 * 60 * 60 * 24;
		const periodSharder = lib.intervalSinceEpoch(oneDayInterval, +Date.now() - (10 * oneDayInterval));
		const keyMaker = lib.getKey('ztime:incr_test:%s');

		it('is properly defined', function() {
			assert.equal(typeof lib.increment, 'function');
			assert.equal(lib.increment.length, 4);
		});

		const incr = lib.increment(redis, periodSharder, keyMaker, oneDayInterval);

		it('is curried', function() {
			assert.equal(typeof incr, 'function');
			assert.equal(incr.length, 3);
		});

		it('defaults to increment +1 on the current slice of time', function(done) {
			incr('default').then(function(result) {
				assert.equal(result, 1);
				return incr('default');
			}).then(function(result) {
				assert.equal(result, 2);
				redis.zrange(['ztime:incr_test:10', '0', '-1', 'WITHSCORES'], function(err, result) {
					if (err) {
						done(err);
					} else {
						assert(Array.isArray(result));
						assert.equal(result.length, 2);
						assert.equal(result[0], 'default');
						assert.equal(result[1], '2');
						done();
					}
				});
			}).catch(done);
		});

		it('supports custom increments', function(done) {
			incr('custom', 2).then(function(result) {
				assert.equal(result, 2);
				redis.zrange(['ztime:incr_test:10', '0', '-1', 'WITHSCORES'], function(err, result) {
					if (err) {
						done(err);
					} else {
						assert(Array.isArray(result));
						assert.equal(result.length, 4);
						assert.equal(result[0], 'custom');
						assert.equal(result[1], '2');
						done();
					}
				});
			});
		});

		it('supports increments to any time-slice', function(done) {
			var tomorrow = oneDayInterval + Date.now();
			incr('default', 1, tomorrow).then(function(result) {
				assert.equal(result, 1);
				redis.zrange(['ztime:incr_test:11', '0', '-1', 'WITHSCORES'], function(err, result) {
					if (err) {
						done(err);
					} else {
						assert(Array.isArray(result));
						assert.equal(result.length, 2);
						assert.equal(result[0], 'default');
						assert.equal(result[1], '1');
						done();
					}
				});
			});
		});

		it('adds a TTL to the zsets', function() {
			redis.ttl('ztime:incr_test:11', function(err, result) {
				if (err) {
					done(err);
				} else {
					assert.equal(typeof result, 'number');
					assert(result > 0);
					done();
				}
			});
		});
	});

	describe('#getRange', function() {
		const oneDayInterval = 1000 * 60 * 60 * 24;
		const epoch = +Date.now() - (10 * oneDayInterval);
		const periodSharder = lib.intervalSinceEpoch(oneDayInterval, epoch);
		const keyMaker = lib.getKey('ztime:incr_test:%s');

		it('is properly defined', function() {
			assert.equal(typeof lib.getRange, 'function');
			assert.equal(lib.getRange.length, 5);
		});

		it('returns an array of redis keys for a given range', function() {
			var from = new Date(epoch),
				to = new Date(epoch + (20 * oneDayInterval));
			var range = lib.getRange(periodSharder, keyMaker, new Interval('PT24H'), from, to);
			assert(Array.isArray(range));
			assert.equal(range.length, 21);
			assert.equal(range[0], 'ztime:incr_test:0');
			assert.equal(range[20], 'ztime:incr_test:20');
		});
	});

	// this test suite depends on #increment's test suite
	describe('#get', function() {
		const oneDayInterval = 1000 * 60 * 60 * 24;
		const epoch = +Date.now() - (10 * oneDayInterval);
		const periodSharder = lib.intervalSinceEpoch(oneDayInterval, epoch);
		const keyMaker = lib.getKey('ztime:incr_test:%s');

		it('is properly defined', function() {
			assert.equal(typeof lib.get, 'function');
			assert.equal(lib.get.length, 4);
		});

		it('is curried', function() {
			assert.equal(lib.get(redis, periodSharder, keyMaker, new Interval('PT24H')).length, 3); // last arg is optional
		});

		const _get = lib.get(redis, periodSharder, keyMaker, new Interval('PT24H'));
		it('returns all stats for a given slice', function(done) {	
			_get(new Date(+Date.now() - oneDayInterval), new Date()).then(function(results) {
				assert.equal(typeof results, 'object');
				assert(results.hasOwnProperty('custom'));
				assert(results.hasOwnProperty('default'));
				assert.equal(results.custom, 2);
				assert.equal(results.default, 2);
				done();
			}).catch(done);
		});

		it('aggregates stats when looking up multiple slices', function(done) {
			var from = new Date(epoch),
				to = new Date(epoch + (20 * oneDayInterval));		

			_get(from, to).then(function(results) {
				assert.equal(typeof results, 'object');
				assert(results.hasOwnProperty('custom'));
				assert(results.hasOwnProperty('default'));
				assert.equal(results.custom, 2);
				assert.equal(results.default, 3);
				done();
			}).catch(done);
		});

		it('supports looking up a specified "dimension"', function(done) {
			var from = new Date(epoch),
				to = new Date(epoch + (20 * oneDayInterval));		

			_get(from, to, 'custom').then(function(results) {
				assert.equal(results, 2);
				done();
			}).catch(done);
		});
	});
});

describe('constructor', function() {
	const constructor = require('../src/index.js');

	it('is properly defined', function() {
		assert.equal(typeof constructor, 'function');
		assert.equal(constructor.length, 5);
	});

	it('takes string as arguments', function() {
		var ztime = constructor(redis, 'ztime:constructor_test:%s', 'PT24H', '2014-01-01', 'P1M');
		assert(ztime);
	});

	it('takes objects as arguments', function() {
		var ztime = constructor(redis, 'ztime:constructor_test:%s', new Interval('PT24H'), new Date('2014-01-01'), new Interval('P1M'));
		assert(ztime);
	});

	it('defaults to no TTL', function(done) {
		var ztime = constructor(redis, 'ztime:constructor_test:%s', 'PT24H', new Date());
		ztime.incr('nottl').then(function() {
			redis.ttl('ztime:constructor_test:0', function(err, result) {
				if (err) {
					done(err);
				} else {
					assert.equal(result, -1);
					done();
				}
			});
		});
	});

	describe('#incr', function() {
		it('is properly defined', function() {
			var ztime = constructor(redis, 'ztime:constructor_test:%s', 'PT24H', '2014-01-01', 'P1M');
			assert.equal(typeof ztime.incr, 'function');
			assert.equal(ztime.incr.length, 3); // incr(identity [, value [, time]])
		});
	});

	describe('#get', function() {
		it('is properly defined', function() {
			var ztime = constructor(redis, 'ztime:constructor_test:%s', 'PT24H', '2014-01-01', 'P1M');
			assert.equal(typeof ztime.get, 'function');
			assert.equal(ztime.get.length, 3); // get(from, to [, identity])
		});
	});
});